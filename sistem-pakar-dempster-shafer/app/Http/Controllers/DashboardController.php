<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penyakit;
use App\Models\Gejala;
use App\Models\Hasil;


class DashboardController extends Controller
{
    public function index()
    {
        $jumlah_penyakit = Penyakit::all()->count();
        $jumlah_gejala = Gejala::all()->count();
        $jumlah_riwayat = Hasil::all()->count();


        return view('dashboard', compact('jumlah_penyakit', 'jumlah_gejala', 'jumlah_riwayat'));
    }
}
