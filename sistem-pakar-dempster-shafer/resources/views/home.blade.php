@extends('layout/user')

@section('hero')
    <section id="hero-img" class="d-flex align-items-center">
    <div class="container">
      <h1>Selamat datang di Medister</h1>
      <h6>Periksa gejala anda dan cari tahu apa yang bisa menyebabkannya dengan cepat dan gratis.</h6>
      <a href="#why-us" class="btn-get-started scrollto">Selengkapnya</a>
    </div>
</section><!-- End Hero -->
@endsection

@section('content')
<main id="main">
    <!-- ======= Why Us Section ======= -->
      <section id="why-us" class="why-us">
        <div class="container">

            <div class="col-lg d-flex align-items-stretch">
              <div class="icon-boxes d-flex flex-column justify-content-center">
                <div class="row">
                  @foreach ($listpenyakit as $item)                                   
                  <div class="col-xl-3 d-flex align-items-stretch">
                    <div class="icon-box mt-4 mt-xl-0">
                      <i class="fa-solid fa-disease"></i>
                      <h4>{{ $item->nama_penyakit }}</h4>
                      <p>{{ $item->definisi_penyakit }}</p>
                    </div>
                  </div>
                  @endforeach
                  </div>
              </div><!-- End .content-->
            </div>
          </div>

        </div>
      </section>
    <!-- End Why Us Section -->

  <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container">
        <div class="section-title mb-3">
          <h2>Sebaran Covid</h2>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6 text-light">
            <div class="count-box bg-danger rounded-pill">
              <i class="fas fa-user-md bg-danger"></i>
              <span data-purecounter-start="0"  data-purecounter-end="{{ $datacovid['cases'] }}" data-purecounter-duration="1" class="text-light purecounter"></span>
              <p>Kasus Positif</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 text-light">
            <div class="count-box bg-success rounded-pill">
              <i class="fas fa-user-md bg-success"></i>
              <span data-purecounter-start="0" data-purecounter-end="{{ $datacovid['recovered'] }}" data-purecounter-duration="1" class="text-light purecounter"></span>
              <p>Pasien Sembuh</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 text-light">
            <div class="count-box bg-warning rounded-pill">
              <i class="fas fa-user-md bg-warning"></i>
              <span data-purecounter-start="0" data-purecounter-end="{{ $datacovid['deaths'] }}" data-purecounter-duration="1" class="text-light purecounter"></span>
              <p>Kasus Meninggal</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- End Counts Section -->

  <!-- ======= Berita Section ======= -->
    <section id="berita" class="testimonials">
      <div class="container">
        <div class="section-title" style="margin-bottom: -50px">
          <h2>Seputar Kesehatan</h2>
        </div>
        {{-- <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
            @foreach ($datanews as $item)        
            <div class="swiper-slide">
              <div class="testimonial-wrap">
                <div class="testimonial-item">
                  <img class="testimonial-img" src="{{ $item['urlToImage'] }}" alt="">
                  <h3>{{ Str::limit($item['title'], 75) }}</h3>
                  <p>{{ Str::limit($item['description'], 100) }}</p>
                  <h4>{{ $item['source']['name'] }} - {{ date('Y-m-d H:i:s', strtotime($item['publishedAt'])) }}</h4>
                  <p>
                    <a href="{{ $item['url'] }}" target="_blank">Cek berita selengkapnya disini!</a>
                  </p>
                </div>
              </div>
            </div><!-- End testimonial item -->
            @endforeach
          </div>
          <div class="swiper-pagination"></div>
        </div> --}}
        
        <div class="row">
          @foreach ($datanews as $item)
          <div class="card col-md-3 border-0 mt-5">
            <div class="inner">
              <img src="{{ $item['urlToImage'] }}" class="card-img-top img-fix-height" alt="Gambar Berita">
            </div>
            <small class="card-title text-muted">{{ $item['source']['name'] }} - {{ date('Y-m-d H:i:s', strtotime($item['publishedAt'])) }}</small>
            <a href="{{ $item['url'] }}" target="_blank" class="text-dark">
              <p class="card-text">{{ Str::limit($item['title'], 75) }}</p>
            </a>
          </div>
          @endforeach
        </div>

      </div>
    </section>
  <!-- End Berita Section -->

  <!-- ======= Periksa Fakta Section ======= -->
    <section id="periksafakta" class="gallery">
      <div class="container">

        <div class="section-title">
          <h2>Berita Hoax</h2>
        </div>
      </div>

      <div class="container">
        <div class="row g-0">

          @foreach ($datahoax as $item)
              
          <div class="col-lg-3 col-md-4">
            <div class="card-body">
              <a href="{{ $item['url'] }}" target="_blank">
                {{ $item['title'] }}
              </a>
            </div>
          </div>

          @endforeach

        </div>

      </div>
    </section>
  <!-- End Periksa Fakta Section -->


    <!-- ======= Rujukan Section ======= -->
    <section id="rujukan" class="doctors">
      <div class="container">

        <div class="section-title">
          <h2>Rumah Sakit Rujukan</h2>
          <p>Daftar Rumah Sakit Rujukan COVID-19 di Indonesia</p>
        </div>

        <div class="row">
          @foreach ($datahospitals as $item)
          <div class="col-lg-6 mb-3">
            <div class="member d-flex align-items-start">
              <div class="member-info">
                <h4>{{ $item['name'] }}</h4>
                <span>{{ $item['region'] }}</span>
                <p>{{ $item['address'] }} <strong>{{ $item['phone'] }}</strong></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>

      </div>
    </section>
    <!-- End Rujukan Section -->
</main><!-- End #main -->
@endsection