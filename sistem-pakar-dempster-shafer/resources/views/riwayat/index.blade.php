@extends('layout.admin')

@section('content-header')
    <h1 class="h3 mb-4 text-gray-800">
        <i class="fa-solid fa-list"></i> Riwayat Konsultasi
    </h1>
@endsection

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Riwayat</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead class="bg-primary text-light">
              <tr>
                <th>Nama</th>
                <th>Diagnosa</th>
                <th>Persentase</th>
                <th>Tanggal & Waktu</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($listhasil as $item)                                              
                <tr>
                  <td width="10%">{{ $item->nama }}</td>
                  <td class="text-justify">{{ $item->diagnosa }}</td>
                  <td class="text-justify">{{ $item->persentase }}</td>
                  <td class="text-justify">{{ $item->created_at }}</td>
                  <td width="10%">
                    <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal_hapus_hasil{{ $item->id }}"><i class="fa-solid fa-trash"></i></button>
                  </td>
                </tr>
              @empty
                                            
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>

    {{ $listhasil->links() }}

{{-- Modal Hapus Penyakit --}}
@foreach ($listhasil as $item)
<div class="modal fade" id="modal_hapus_hasil{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Hapus Riwayat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <h5 class="text-center">Anda yakin ingin menghapus riwayat ini?</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <form action="/hasil/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')        
              <input type="submit" name="delete" id="delete" value='Hapus' class="btn btn-danger btn-sm">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endforeach
{{-- Akhir Modal Hapus Penyakit --}}

@endsection